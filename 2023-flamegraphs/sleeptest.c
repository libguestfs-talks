#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>

#define NR_THREADS 32

static void *
busywork_thread (void *start_vp)
{
  struct timeval *start_t = start_vp;
  struct timeval t;
  int i;
  unsigned a[256] = { 0 };

  while (1) {
    gettimeofday (&t, NULL);
    if (t.tv_sec >= start_t->tv_sec + 3 ||
        (t.tv_sec == start_t->tv_sec + 2 &&
         t.tv_usec >= start_t->tv_usec))
      break;

    for (i = 0; i < 256; ++i) {
      a[i] += t.tv_usec;
      if (i > 0) a[i] += a[i-1];
    }
  }
  return NULL;
}

static void
busywork_2secs (void)
{
  pthread_t thr[NR_THREADS];
  int i;
  struct timeval start_t;

  gettimeofday (&start_t, NULL);

  for (i = 0; i < NR_THREADS; ++i)
    pthread_create (&thr[i], NULL, busywork_thread, &start_t);
  for (i = 0; i < NR_THREADS; ++i)
    pthread_join (thr[i], NULL);
}

int
main (void)
{
  int i;

  for (i = 0; i < 5; ++i) {
    busywork_2secs ();
    sleep (6);
  }
  return 0;
}
