#!/bin/bash -

disk="$1"

guestfish --rw -a "$disk" -i <<EOF
  rm /etc/udev/rules.d/70-persistent-net.rules
EOF
