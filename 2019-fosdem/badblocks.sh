#!/bin/bash

case "$1" in
    get_size) echo 64M ;;
    pread)
        if [ $4 -le 100000 ] && [ $(( $4+$3 )) -gt 100000 ]; then
            echo EIO Bad block >&2
            exit 1
        else
            dd if=/dev/zero count=$3 iflag=count_bytes
        fi ;;
    *) exit 2 ;;
esac
