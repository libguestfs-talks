#!/bin/bash
set -x
set -e

source settings.sh

qemu-img create -f qcow2 -b ssh://$remote/$dir/fedora-33.img snapshot.qcow2
qemu-img convert -f qcow2 snapshot.qcow2 -O raw local.img
