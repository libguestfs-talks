#!/usr/bin/python
import os
import guestfs

output = "disk.img"
f = open (output, "w")
f.truncate (512 * 1024 * 1024)
f.close ()

g = guestfs.GuestFS ()
g.add_drive (output)
g.launch ()
devices = g.list_devices ()
g.part_disk (devices[0], "mbr")
parts = g.list_partitions ()
g.mkfs ("ext4", parts[0])
g.mount (parts[0], "/")
g.tgz_in ("data.tar.gz", "/")
g.shutdown ()
g.close ()
