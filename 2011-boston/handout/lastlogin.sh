#!/bin/bash -

#set -x

winkey='HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon'
winval=DefaultUserName

# List of guests.
for name in $(virsh -q list --all | awk '{print $2}'); do
    # What guest is it?
    eval $(guestfish --listen --ro -d "$name" -i)
    root=$(guestfish --remote "inspect-get-roots")
    type=$(guestfish --remote inspect-get-type "$root")
    guestfish --remote exit
    # Depending on the guest type ...
    case "$type" in
        windows)
            echo -n "$name "
            virt-win-reg "$name" "$winkey" "$winval"
            ;;
        linux)
            echo -n "$name "
            virt-cat -d "$name" /var/log/wtmp > /tmp/wtmp
            last -f /tmp/wtmp | \
                grep -Ev "^reboot|^shutdown|^wtmp begins|^$" | \
                tail -1 | \
                awk '{print $1}'
            ;;
        *)
            echo -n "($name - unknown guest type, ignored)"
            ;;
    esac
done