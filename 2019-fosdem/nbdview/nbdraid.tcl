#!/usr/bin/env wish
# Visualize RAID activity - read the README file first!
# Copyright (C) 2018 Red Hat Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# * Neither the name of Red Hat nor the names of its contributors may be
# used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY RED HAT AND CONTRIBUTORS ''AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL RED HAT OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

package require Tk

source "nbdcanvas.tcl"

set blocksize 4096
set width 128

# For 1024x768 and 1280x1024 use scale 1
# For larger screens use scale 2
if { [winfo screenheight .] > 1024 } {
    set scale 2
} else {
    set scale 1
}

# This list is used to store all the RAID handles.
set handles {}

# This function polls the log file of each handle (every 100 ms).
proc poll {} {
    global handles

    foreach h $handles {
        nbdpoll $h
    }
    after 100 poll
}

# Parse command line.
if { $argc != 4 } {
    puts "usage: nbdview N size logfile-pattern errorfile-pattern"
    exit 1
}
set N [lindex $argv 0]
set size [lindex $argv 1]
set logpattern [lindex $argv 2]
set errorpattern [lindex $argv 3]

# Menubar.
option add *tearOff 0
menu .menubar
. configure -menu .menubar
menu .menubar.file
.menubar add cascade -menu .menubar.file -label File
.menubar.file add command -label "Quit" -command { exit }

# Frame.
frame .f
pack .f -anchor center -expand 1 -fill both -ipadx 10 -ipady 10

# Canvases.
for { set i 1 } { $i <= $N } { incr i } {
    set logfile($i) [format $logpattern $i]
    set errorfile($i) [format $errorpattern $i]

    # VBox to store the canvas and controls.
    set box ".f.h$i"
    frame $box
    pack $box -side left -expand 1 -fill both

    # Create the error checkbox.
    set errorbutton "$box.e"
    set errorvar($i) off
    checkbutton $errorbutton -text "Error" -variable errorvar($i) \
        -command "set_errorfile $i"
    proc set_errorfile { i } {
        global errorvar errorfile
        if { $errorvar($i) } {
            # Touch the error inject file.
            close [open $errorfile($i) "w"]
        } else {
            # Delete it.
            file delete $errorfile($i)
        }
    }
    pack $errorbutton -side top -anchor nw -expand 0 -fill none

    # Create the i'th canvas.
    set canvas "$box.c"
    set h [nbdcanvas $canvas $logfile($i) $size $blocksize $width $scale]
    $canvas configure -borderwidth 4 -relief groove
    pack $canvas -side top -expand 0 -fill none

    lappend handles $h
}

# Set up the window.
wm attributes . -topmost 1
wm title . "nbdraid"
wm resizable . false false

# Start polling the log file.
after 100 poll
