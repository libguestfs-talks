#!/bin/bash
set -x
set -e

source settings.sh

nbdkit -f --exit-with-parent --filter=tar file $ova tar-entry=fedora-33.img &
sleep 0.5
qemu-img create -f qcow2 -b nbd:localhost:10809 snapshot.qcow2
virt-sparsify --inplace snapshot.qcow2
qemu-img convert -f qcow2 snapshot.qcow2 -O raw local.img
