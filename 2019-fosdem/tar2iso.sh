#!/bin/bash

case "$1" in
    config)
        [ "$2" = "file" ] && ln -sf "$3" $tmpdir/tarfile  ;;
    config_complete)
        tar zxf $tmpdir/tarfile -C $tmpdir
        rm $tmpdir/tarfile
        mkisofs -JrT -o $tmpdir/iso $tmpdir ;;
    open) ;;
    get_size)
        stat -c '%s' $tmpdir/iso ;;
    pread)
        dd iflag=skip_bytes,count_bytes skip=$4 count=$3 if=$tmpdir/iso ;;
    *) exit 2 ;;
esac
