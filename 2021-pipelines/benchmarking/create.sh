#!/bin/bash
set -x
set -e

source settings.sh

file=$dir/fedora-33.img

virt-builder fedora-33 --size 32G -o $file
virt-customize -a $file \
    --run-command 'dd if=/dev/urandom of=/var/tmp/big bs=1M count=3000 &&
                   sync &&
                   rm /var/tmp/big'
