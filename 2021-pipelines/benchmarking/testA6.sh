#!/bin/bash
set -x
set -e

source settings.sh

nbdkit -f --exit-with-parent --filter=cow ssh host=$remote $dir/fedora-33.img &
sleep 1
guestfish <<EOF
  add "" protocol:nbd server:tcp:localhost discard:enable format:raw
  run
  mount-options discard /dev/sda2 /
  fstrim /
  umount /
  mount-options discard /dev/sda3 /
  fstrim /
  umount /
EOF
nbdcopy nbd://localhost local.img
