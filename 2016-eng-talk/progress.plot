set title "Appliance boot time vs development effort"
set term pdf
set output "progress.pdf"
set xdata time
set style data lines
set timefmt "%Y-%m-%d"
set xlabel "Development timeline"
set ylabel "Boot time (ms)"
set xrange ["2016-03-01" : "2016-06-01"]
set yrange [0 : 4000]
set format x "%d %b"
plot "progress.data" using 1:2 t "Appliance boot time"
