#!/bin/bash -
source functions
cd empty
add_history virt-inspector --yaml /dev/vg_thinkpad/Windows7x32 "|" less
add_history virt-inspector /dev/vg_thinkpad/Debian5x32 "|" less
add_history ls -l /dev/vg_thinkpad
terminal --title="Virt-inspector"
